<?php
/**
 * Main class of module x13imageregenerator
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 */

class AdminImageregeneratorController extends ModuleAdminController
{
    public $_html;
    public $_html_content;
    public $images_type;
    public $images_size;
    public $_ajax;
    public $mode;
    public $start_time;
    
    public $i=0;
    public $j=0; // ??;
    public $z=0;
    public $y=0;
    public $types = array();


    public $counter=0;
    public $counter_product = 0;
    public $counter_others;
    public $logs;
    public $response = array();
    
    
    public $max_execution_time;
    public $ps_version;
    
    public function __construct()
    {
        $this->max_execution_time=0;
        $this->bootstrap = true;
        
        $this->counter = '0';
        $this->counter_others = '0';
        
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);
        
        
        parent::__construct();



        $this->images_type = array(
            'products',
            'categories',
            'suppliers',
            'manufacturers',
            'stores',
        );
        
        $this->_html .= '<style type="text/css">#fieldset_0_6 {display:none;} </style>';
        $this->_html .= '<div class="panel panel-default">';
        
        $this->_html .= '<ul class="nav nav-tabs" id="myTab" role="tablist">';
        $this->_html .= '<li class="nav-item active">';
        $this->_html .= '<a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">Wszystkie</a>';
        $this->_html .= '</li>';
        $this->_html .= '<li class="nav-item">';
        $this->_html .= '<a class="nav-link" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Produkty</a>';
        $this->_html .= '</li>';
        $this->_html .= '<li class="nav-item">';
        $this->_html .= '<a class="nav-link" id="categories-tab" data-toggle="tab" href="#categories" role="tab" aria-controls="categories" aria-selected="false">Kategorie</a>';
        $this->_html .= '</li>';
        $this->_html .= '<li class="nav-item">';
        $this->_html .= '<a class="nav-link" id="stores-tab" data-toggle="tab" href="#stores" role="tab" aria-controls="stores" aria-selected="false">Sklepy</a>';
        $this->_html .= '</li>';
        $this->_html .= '<li class="nav-item">';
        $this->_html .= '<a class="nav-link" id="manufacturers-tab" data-toggle="tab" href="#manufacturers" role="tab" aria-controls="manufacturers" aria-selected="false">Producenci</a>';
        $this->_html .= '</li>';
        $this->_html .= '<li class="nav-item">';
        $this->_html .= '<a class="nav-link" id="suppliers-tab" data-toggle="tab" href="#suppliers" role="tab" aria-controls="suppliers" aria-selected="false">Dostawcy</a>';
        $this->_html .= '</li>';
        $this->_html .= '</ul>';

        $this->_html .= '<div class="tab-content" id="myTabContent">';
        $this->_html .= '<div class="tab-pane fade active in" id="all" role="tabpanel" aria-labelledby="all-tab">'.$this->ModerenderForm('all').'</div>';
        $this->_html .= '<div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">'.$this->ModerenderForm('products').'</div>';
        $this->_html .= '<div class="tab-pane fade" id="categories" role="tabpanel" aria-labelledby="categories-tab">'.$this->ModerenderForm('categories').'</div>';
        $this->_html .= '<div class="tab-pane fade" id="stores" role="tabpanel" aria-labelledby="stores-tab">'.$this->ModerenderForm('stores').'</div>';
        $this->_html .= '<div class="tab-pane fade" id="manufacturers" role="tabpanel" aria-labelledby="manufacturers-tab">'.$this->ModerenderForm('manufacturers').'</div>';
        $this->_html .= '<div class="tab-pane fade" id="suppliers" role="tabpanel" aria-labelledby="suppliers-tab">'.$this->ModerenderForm('suppliers').'</div>';
        $this->_html .= '</div>';

        $this->_html .= '</div>';

        $this->_html .= '<div class="panel" id="generatorImagesStatus"><div class="panel-heading">
        ' . $this->module->l('Generating images') . '</div><ul></ul>';

        $this->_html .= '<div id="radiusStatusGenerate"><div class="progress mx-auto" data-value="0">';
          $this->_html .= '<span class="progress-left">';
                        $this->_html .= '<span class="progress-bar border-success" style="transform: rotate(0deg);"></span>';
          $this->_html .= '</span>';
          $this->_html .= '<span class="progress-right">';
                        $this->_html .= '<span class="progress-bar border-success" style="transform: rotate(0deg);"></span>';
          $this->_html .= '</span>';
          $this->_html .= '<div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">';
            $this->_html .= '<div class="h2 font-weight-bold"><span id="procent_all">0</span><sup class="small">%</sup></div>';
          $this->_html .= '</div>';
        $this->_html .= '</div></div>';
        
        $this->_html .= '<div class="alert alert-info" role="alert"><p class="alert-text">'.$this->module->l('Dont leave this page while completed generating images').'</p></div>';
        
        $this->_html .= '<table class="table">';
        $this->_html .= '<tr>';
        $this->_html .= '<td>';
        $this->_html .= 'Image Type:';
        $this->_html .= '</td>';
        $this->_html .= '<td>';
        $this->_html .= 'Progress:';
        foreach ($this->images_type as $type) {
            $this->_html .= '<tr>';
            $this->_html .= '<td style="width:30%;">';
            $this->_html .= $type;
            $this->_html .= '</td>';
            $this->_html .= '<td>';
            $this->_html .= '<div class="progress">'; 
            if(($this->getCountImageByType($type)) == 0) {
                $this->_html .= '<div class="progress-bar progress-bar-striped progress-bar-animated disabled" role="progressbar" data-type="'.$type.'" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">';
            } else {
                $this->_html .= '<div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" data-type="'.$type.'" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">';
            }
            $this->_html .= '<span class="count_percent">100</span>/<span>100</span></div>';
            $this->_html .= '</div>';
            
            $this->_html .= '</td>';
            
            $this->_html .= '<td style="width:10%;">';
            $this->_html .= '<span class="current_Count_image" data-type="'.$type.'">0</span>/<span class="all_Type_images" data-type="'.$type.'">'.($this->getCountImageByType($type)).'</span>';
            $this->_html .= '</td>';
            $this->_html .= '</tr>';
        }
        $this->_html .= '</td>';
        $this->_html .= '</tr>';
        
        $this->_html .= '</table>';
        
        $this->_html .= '</div>';

        $this->_html .= '<div class="panel" id="imageRegeneratorLogs" style="display:block;"><div class="panel-heading">
        ' . $this->module->l('Logs') . '</div>'.$this->statusForm().'<ul id="logiListaX13Regenerate"></ul></div>';
        

            
      $this->table = 'image_type';
        $this->className = 'ImageType';
        $this->identifier = 'filename';

    }

    public function ajaxProcessImageregenerate()
    {
        $this->response['x13imageregenerator_mode'] = Tools::getValue('x13imageregenerator_mode');
        $this->response["message"][] = "";
        $this->Imageregenerate(Tools::getValue('x13imageregenerator_mode'), Tools::getValue('x13imageregenerator_size'),Tools::getValue("x13imageregenerator_counter"),Tools::getValue("x13imageregenerator_counter_product"), Tools::getValue('x13imageregenerator_watermark'));
    }

   
    
    public function initContent()
    {
        return parent::initContent().$this->_html.$this->statusForm();
    }
 
    public function renderList()
    {
        return $this->_html. parent::renderList();
    }
    
    public function postProcess()
    {
        parent::postProcess();
        
        if (Tools::isSubmit('submitAddconfiguration')) {

            Configuration::updateValue('x13imageregenerator_mode', serialize(Tools::getValue('x13imageregenerator_mode')));
//
            $this->confirmations[] = "Successful message";
        }
    }
    public function Imageregenerate($type, $sizes, $counter, $counter_product, $watermark) { 
//        $this->i=0;
//        $this->j=0;
        
        // -- Za kazdym odpaleniem ajaxa zlicza mi wszystkie zdjecia -- 
        $counter_all = 0;
        if($type == 'all') {
            foreach($this->images_type as $t) {
                (int)$counter_all = (int)$counter_all + (int)$this->getCountImageByType($t);
            }
        } else {
                (int)$counter_all = (int)$counter_all + (int)$this->getCountImageByType($type);
        }
        $this->response["counter_all"] = $counter_all;

        //$this->response["test1"] ="OK";
        // When moving images, if duplicate images were found they are moved to a folder named duplicates/
        if (file_exists(_PS_PROD_IMG_DIR_ . 'duplicates/')) {
            $this->warnings[] = $this->trans('Duplicate images were found when moving the product images. This is likely caused by unused demonstration images. Please make sure that the folder %folder% only contains demonstration images, and then delete it.', array('%folder%' => _PS_PROD_IMG_DIR_ . 'duplicates/'), 'Admin.Design.Notification');
        }
   
        $this->_regenerateThumbnails($type, true, $counter, $counter_product, $watermark);
    }
    
    public function getCountImageByType($type) {
        if($type == 'products') {
         return count(Image::getAllImages());//$this->Allimages;
            //return (count(Product::getProducts($this->context->language->id,0,999999999,'id_product','ASC')));
        }
        if($type == 'categories') {
            return (count(Category::getCategories(false,false,false))-2);
        }
        if($type == 'manufacturers') {
            return (count(Manufacturer::getManufacturers(false,$this->context->language->id)));
        }
        if($type == 'suppliers') {
            return (count(Supplier::getSuppliers(false,false,false)));
        }
        if($type == 'stores') {
            return (count(Store::getStores(false)));
        }
    }
    
   
    
    public function ModerenderForm($type) {
        if($type == 'all') {
            $this->mode = 'all';
        }
        if($type == 'products') {
            $this->mode = 'products';
        }
        if($type == 'categories') {
            $this->mode = 'categories';
        }
        if($type == 'stores') {
            $this->mode = 'stores';
        }
        if($type == 'manufacturers') {
            $this->mode = 'manufacturers';
        }
        if($type == 'suppliers') {
            $this->mode = 'suppliers';
        //}
              $this->_ajax = '<script type="text/javascript">

var getServerStatus;
var xhr;
var myinterval;
var counter = 0;
var postdata;

        $("#all_InitGenerating, #products_InitGenerating, #manufacturers_InitGenerating, #suppliers_InitGenerating, #stores_InitGenerating, #categories_InitGenerating").click(function() {
    
        console.log("init");
clearInterval(myinterval);
$("#logiListaX13Regenerate").html("");

$("#x13imageregenerator_counter").val("0");
$("#x13imageregenerator_counter_product").val("0");
$("#x13imageregenerator_counter_all").val("1");


$("#logiListaX13Regenerate").append("<li>START GENEROWANIA OBRAZKÓW</li>");

$(".current_Count_image").each(function() {
    $(".current_Count_image").html("0");
    $(".count_percent").html("0");
    $(".table tr .progress-bar").css("width","0%");
    $("#procent_all").html("0");
    
    $(".progress-right .progress-bar").css("transform","0deg");
    $(".progress-left .progress-bar").css("transform","0deg");
});


//console.log($(this).parent().parent().children().find(".x13imageregenerator_mode").val());

  var response_categories = 0;
  var response_products = 0;
  var response_suppliers = 0;
  var response_manufacturers = 0;
  var response_stores = 0;
  
  
  
            myinterval = setInterval(function(){
  console.log("GGG"); 
  console.log($.active);
 if($.active == 0) {
                 postdata = {
                    ajax: 1,
                    controller: "AdminImageregenerator",
                    action: "Imageregenerate",
                    token: token,
                    x13imageregenerator_mode: $("#myTab .nav-item.active").children().attr("aria-controls"),
                    x13imageregenerator_size: $("#x13imageregenerator_mode_"+$("#myTab .nav-item.active").children().attr("aria-controls")).val(),//$(this).parent().prev().find(".x13imageregenerator_mode").val(), //$("#x13imageregenerator_mode").val(),
                    imageRegenerator: "1",
                    x13imageregenerator_counter:$("#x13imageregenerator_counter").val(),
                    x13imageregenerator_counter_product:$("#x13imageregenerator_counter_product").val(),
                    x13imageregenerator_watermark:document.querySelector("#x13imageregenerator_watermark_"+$("#myTab .nav-item.active").children().attr("aria-controls")+"_watermark_"+$("#myTab .nav-item.active").children().attr("aria-controls")).checked
                    
                };

            console.log(postdata);
            $.ajax({
                type: "POST",
//                async: true,
                dataType: "text",
                url: "index.php",
                data: postdata,
                success: function (data) {
                    var response = jQuery.parseJSON(data);
 
                    console.log(response);
                    if((response.message == null)){ // || (response.message == false
                        clearInterval(myinterval);
                        $("#logiListaX13Regenerate").append("Koniec generownaia obrazków");
                    }
                    if(response.message.length > 0) {
                    for(var z=0;z<response.message.length;z++) {
                    if(response.message[z] != "") {
                        $("#logiListaX13Regenerate").append("<li>"+response.message[z]+"</li>");
                        if(response.current_process == "categories") {
                            response_categories += 1;
                        }
                        if(response.current_process == "products") {
                            response_products += 1;
                        }
                        if(response.current_process == "suppliers") {
                            response_suppliers += 1;
                        }
                        if(response.current_process == "manufacturers") {
                            response_manufacturers += 1;
                        }
                        if(response.current_process == "stores") {
                            response_stores += 1;
                        }
                        console.log("pobieram ilosc procesow");
                    }
                    }
                    }
                    

                    console.log("CAT"+response_categories);
                    if(parseInt(response.counter)>=0) {
                        $("#x13imageregenerator_counter").val(response.counter);
                    }
                    if(parseInt(response.counter_product)>=0) {
                        $("#x13imageregenerator_counter_product").val(response.counter_product);
                    }
                    

                    
//                    var getCurrentValue = $(".current_Count_image[data-type="+response.current_process+"]").html();
//                    if(isNaN(getCurrentValue) == true) {
//                        getCurrentValue  = 0;
//                    }
                    
                    //if(response.process.categories == null) { 
                       // var response_categories = 0;
                    //} else {
                    //response_categories = response.process.categories;
                    
                    if((parseInt(response_categories)>=0)&&(parseInt(response_categories) <= parseInt($(".all_Type_images[data-type=categories]").html()))) {
                            $(".current_Count_image[data-type=categories]").html(parseInt(response_categories));
                        }
                    //}
                    //if(response.process.suppliers == null) { 
                        //var response_suppliers = 0;
                   // }  else {
                    //response_suppliers = response.process.suppliers;
                        if((parseInt(response_suppliers)>=0)&& (parseInt(response_suppliers) <= parseInt($(".all_Type_images[data-type=suppliers]").html()))) {
                            $(".current_Count_image[data-type=suppliers]").html(parseInt(response_suppliers));
                        }
                   // }
                   // if(response.process.manufacturers == null) { 
                       // var response_manufacturers = 0;
                   // } else {
                    //response_manufacturers = response.process.manufacturers;
                    if((parseInt(response_manufacturers)>=0)&&(parseInt(response_manufacturers) <= parseInt($(".all_Type_images[data-type=manufacturers]").html()))) {
                            $(".current_Count_image[data-type=manufacturers]").html(parseInt(response_manufacturers));
                        }
                    //}
                    //if(response.process.stores == null) { 
                       // var response_stores = 0;
                    //} else {
                    //response_stores = response.process.stores;
                        if((parseInt(response_stores)>=0)&&(parseInt(response_stores) <= parseInt($(".all_Type_images[data-type=stores]").html()))) {
                            $(".current_Count_image[data-type=stores]").html(parseInt(response_stores));
                        }
                   // }
                   // if(response.process.products == null) { 
                       // var response_products = 0;
                   // } else {
                    //response_products = response.process.products;
                    
                      if((parseInt(response_products)>=0)&&(parseInt(response_products) <= parseInt($(".all_Type_images[data-type=products]").html()))) {
                            $(".current_Count_image[data-type=products]").html(parseInt(response_products));
                        }
                    //}
                    
                    
                    var percentAll=0;
                    if(response.x13imageregenerator_mode == "all") {
                    $(".current_Count_image").each(function() {
                        percentAll += (parseInt($(this).html())/parseInt(response.counter_all))*100;
                        
                    });
                    } else {
                        percentAll = parseInt($(".current_Count_image[data-type="+response.current_process+"]").html())/parseInt(response.counter_all)*100;
                    }
                    
                    
                    
                    $("#procent_all").text(percentAll.toFixed(0));
                    if((percentAll.toFixed(0)>=0) && (percentAll.toFixed(0) <= 50)) {
                    $(".progress-right .progress-bar").css("transform","rotate("+(((percentAll/100))*360)+"deg");
                    } else {
                    $(".progress-right .progress-bar").css("transform","rotate(180deg)");
                    
                    $(".progress-left .progress-bar").css("transform","rotate("+((((percentAll-50)/100))*360)+"deg");
                    }
                    
                    var getPercent = parseInt($(".current_Count_image[data-type="+response.current_process+"]").html())/parseInt($(".all_Type_images[data-type="+response.current_process+"]").html())*100;
                    $(".progress-bar[data-type="+response.current_process+"]").css("width",getPercent.toFixed(0)+"%");
                    if(parseInt($(".all_Type_images[data-type="+response.current_process+"]").html()) == 0) {
                    $(".progress-bar[data-type="+response.current_process+"] .count_percent").html("0");
                    } else{
                    $(".progress-bar[data-type="+response.current_process+"] .count_percent").html(getPercent.toFixed(0));
                    }
                    $("#x13imageregenerator_counter_all").val(response.counter_all);
 
                    console.log("Test");
                    
if(parseInt(percentAll.toFixed(0)) == 100) {
                        clearInterval(myinterval);
                        $("#logiListaX13Regenerate").append("<li>Koniec generownaia obrazków</li>");
                    }
  
                },
            error:function(xhr) {
                console.log(xhr.responseText);
                clearInterval(myinterval);
            }

       });
      }
    },3000); //10000
    });
     //}); 
     
$("#all_StopGenerating, #products_StopGenerating, #manufacturers_StopGenerating, #suppliers_StopGenerating, #stores_StopGenerating, #categories_StopGenerating").click(function() {
                clearInterval(myinterval);
      $("#logiListaX13Regenerate").append("<li>ZATRZYMANIE GENEROWANIA OBRAZKÓW</li>");
});
 
 
</script>';
              
            }
       return $this->renderForm() . $this->_ajax;
    }
    public function renderForm()
    {

        $fields_form = array();

        $this->images_size = (new ImageType())->getImagesTypes(); 
        
        foreach($this->images_size as $key => $size) { 
            $mode[] = array(
                'value' => $size['name'],
                'name' => $size['name']
            );
        }

        $table_inline = '';
 
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->module->l('Sizes'),
                    'name' => 'x13imageregenerator_mode[]',
                    'cols' => 5,
                    'id' => 'x13imageregenerator_mode_'.$this->mode,
                    'multiple' => true,
                    'desc' => $this->module->l('You can choose multiple options by click CTRL'),
                    'class' => 'form-control x13imageregenerator_mode',
                    'options' => array(
                        'query' => $mode,
                        'id' => 'value',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'checkbox',
                    'label' => $this->module->l('Watermark'),
                    'desc' => $this->l('Generate images with watermark'),
                    'name' => 'x13imageregenerator_watermark_'.$this->mode,
                    'values' => array(
                        'query' => array(
                            array(
                                'id' => 'watermark_'.$this->mode,
                                'name' => $this->l('Show watermark'),
                                'val' => '1',
                                'checked' => 'checked'
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    )
                )
            ),
            'buttons' => array(
                'save' => array(
                    'title' => $this->l('Run'),
                    'name' => 'initGenerating',
                    'type' => 'button',
                    'id' => $this->mode.'_InitGenerating',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save',
                ),
                'stop' => array(
                    'title' => $this->l('Stop'),
                    'name' => 'stopGenerating',
                    'type' => 'button',
                    'id' => $this->mode.'_StopGenerating',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-cancel',
                ),
            )
        );

        $form = new HelperForm();
        $this->fields_form = array();
        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->submit_action = 'initGenerating';
 
        
        $form->tpl_vars['fields_value']['x13imageregenerator_mode[]'] = Tools::getValue('x13imageregenerator_mode[]', Configuration::get('x13imageregenerator_mode'));

        return $form->generateForm($fields_form);
    }
    public function statusForm() { 
        
        $fields_form = array();
        
        $fields_form[0]['form'] = array(
            'input' => array(
                 array(
                    'type' => 'hidden',
                    'label' => $this->module->l('Counter'),
                    'name' => 'x13imageregenerator_counter',
                    'class' => 'x13imageregenerator_counter',
                    'value' => '0'
                ),
                array(
                    'type' => 'hidden',
                    'label' => $this->module->l('Counter_product'),
                    'name' => 'x13imageregenerator_counter_product',
                    'class' => 'x13imageregenerator_counter_product',
                    'value' => '0'
                ),
                array(
                    'type' => 'hidden',
                    'label' => $this->module->l('Counter_All'),
                    'name' => 'x13imageregenerator_counter_all',
                    'class' => 'x13imageregenerator_counter_all',
                    'value' => '1'
                ),
            ),
        );
         
        
        $form = new HelperForm();
        $this->fields_form = array();
        $form->token = Tools::getAdminTokenLite('AdminModules');
        
        $form->tpl_vars['fields_value']['x13imageregenerator_counter_all'] = Tools::getValue('x13imageregenerator_counter_all', '1');
        $form->tpl_vars['fields_value']['x13imageregenerator_counter'] = Tools::getValue('x13imageregenerator_counter', '0');
        $form->tpl_vars['fields_value']['x13imageregenerator_counter_product'] = Tools::getValue('x13imageregenerator_counter_product', '0');
        return $form->generateForm($fields_form);
    }

    protected function _regenerateThumbnails($type, $deleteOldImages, $counter, $counter_product, $watermark)
    {
        $this->response["test2"] ="OK";
        $this->start_time = time();
        ini_set('max_execution_time', $this->max_execution_time); // ini_set may be disabled, we need the real value
        $this->max_execution_time = (int) ini_get('max_execution_time');
        $languages = Language::getLanguages(false);

        $process = array(
            array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
            array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
            array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
            array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_),
            array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
        );

        // Launching generation process
        foreach ($process as  $key => $proc) {
            $this->response['statuss'] = $this->i." + ".$proc['type']." + ".$proc['dir'];
            
            if ($type != 'all' && $type != $proc['type']) {
                continue;
            }
            
            $this->response['current_process'] = $proc['type'];
            
            // Getting format generation
            $formats = ImageType::getImagesTypes($proc['type']);
            ## only for test
//            if ($type != 'all') {
//                $format = (string) (Tools::getValue('format_' . $type));
//                if ($format != 'all') {
//                    foreach ($formats as $k => $form) {
//                        if ($form['id_image_type'] != $format) {
//                            unset($formats[$k]);
//                        }
//                    }
//                }
//            }
            ## only for test
            $this->z=0; // testowo
            $this->y=0; 
            $this->typ = $proc['type'];
 
            
            if ($deleteOldImages) {
                $this->_deleteOldImages($proc['dir'], $formats, ($proc['type'] == 'products' ? true : false));
            }
            if (($return = $this->_regenerateNewImages($proc['dir'], $formats, ($proc['type'] == 'products' ? true : false),$counter, $counter_product)) === true) { //($proc['type'] == 'products' ? true : false)
                if (!count($this->errors)) {
                    $this->errors[] = $this->trans('Cannot write images for this type: %1$s. Please check the %2$s folder\'s writing permissions.', array($proc['type'], $proc['dir']), 'Admin.Design.Notification');
                }
            } elseif ($return == 'timeout') {
                $this->errors[] = $this->trans('Only part of the images have been regenerated. The server timed out before finishing.', array(), 'Admin.Design.Notification');
            } else {
                if ($proc['type'] == 'products') {
                    if($watermark == true) {
                        if ($this->_regenerateWatermark($proc['dir'], $formats) == 'timeout') {
                            $this->errors[] = $this->trans('Server timed out. The watermark may not have been applied to all images.', array(), 'Admin.Design.Notification');
                        }
                    }
                }
                if (!count($this->errors)) {
                    if ($this->_regenerateNoPictureImages($proc['dir'], $formats, $languages)) {
                        $this->errors[] = $this->trans('Cannot write "No picture" image to %s images folder. Please check the folder\'s writing permissions.', array($proc['type']), 'Admin.Design.Notification');
                    }
                }
            }
           // }
           //### $this->i++;
        }

        return count($this->errors) > 0 ? false : true;
    }
    
    
     /* Hook watermark optimization */
    protected function _regenerateWatermark($dir, $type = null)
    {
        $result = Db::getInstance()->executeS('
		SELECT m.`name` FROM `' . _DB_PREFIX_ . 'module` m
		LEFT JOIN `' . _DB_PREFIX_ . 'hook_module` hm ON hm.`id_module` = m.`id_module`
		LEFT JOIN `' . _DB_PREFIX_ . 'hook` h ON hm.`id_hook` = h.`id_hook`
		WHERE h.`name` = \'actionWatermark\' AND m.`active` = 1');

        if ($result && count($result)) {
            $productsImages = Image::getAllImages();
            foreach ($productsImages as $image) {
                $imageObj = new Image($image['id_image']);
                if (file_exists($dir . $imageObj->getExistingImgPath() . '.jpg')) {
                    foreach ($result as $module) {
                        $moduleInstance = Module::getInstanceByName($module['name']);
                        if ($moduleInstance && is_callable(array($moduleInstance, 'hookActionWatermark'))) {
                            call_user_func(array($moduleInstance, 'hookActionWatermark'), array('id_image' => $imageObj->id, 'id_product' => $imageObj->id_product, 'image_type' => $type));
                        }

                        if (time() - $this->start_time > $this->max_execution_time - 4) { // stop 4 seconds before the tiemout, just enough time to process the end of the page on a slow server
                            return 'timeout';
                        }
                    }
                }
            }
        }
    }
     /**
     * Regenerate no-pictures images.
     *
     * @param $dir
     * @param $type
     * @param $languages
     *
     * @return bool
     */
    protected function _regenerateNoPictureImages($dir, $type, $languages)
    {
        $errors = false;
        $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');

        foreach ($type as $image_type) {
            foreach ($languages as $language) {
                $file = $dir . $language['iso_code'] . '.jpg';
                if (!file_exists($file)) {
                    $file = _PS_PROD_IMG_DIR_ . Language::getIsoById((int) Configuration::get('PS_LANG_DEFAULT')) . '.jpg';
                }
                if (!file_exists($dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '.jpg')) {
                    if (!ImageManager::resize($file, $dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '.jpg', (int) $image_type['width'], (int) $image_type['height'])) {
                        $errors = true;
                    }

                    if ($generate_hight_dpi_images) {
                        if (!ImageManager::resize($file, $dir . $language['iso_code'] . '-default-' . stripslashes($image_type['name']) . '2x.jpg', (int) $image_type['width'] * 2, (int) $image_type['height'] * 2)) {
                            $errors = true;
                        }
                    }
                }
            }
        }

        return $errors;
    }
       /**
     * Regenerate images.
     *
     * @param $dir
     * @param $type
     * @param bool $productsImages
     *
     * @return bool|string
     */
    protected function _regenerateNewImages($dir, $type, $productsImages = false, $counter, $counter_product)
    {
        //$this->response["test3"] ="OK";
        if (!is_dir($dir)) {
            return false;
        }

        $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');
        
        // Obrazki wszystkie inne poza produktami ###
        if (!$productsImages) {
            
            $formated_medium = ImageType::getFormattedName('medium');
            
            foreach (scandir($dir, SCANDIR_SORT_NONE) as $image) {

                // i jest iteracja petli a coutner ...? 
                //$counter
               $this->response["warunek_1"] = $this->i." + ".count(scandir($dir, SCANDIR_SORT_NONE));
                 //## test if($counter <= count(scandir($dir, SCANDIR_SORT_NONE))) { // ?? tu jest problem z obrazkami 
               // jezeli ilosc iteracji jest wieksza od ostatnio przetworzonej ilosci\
                if(($this->getCountImageByType($this->typ) % 10) == 0) {
                
                 if($this->i >= $counter) {  
                                    $this->response["test_all"] = "OK";

                 ////ilosc zdjec jest parzysta lub nie
                 //$this->i++;
                     // musi byc kontrola pomiedzy calkowita iloscia obrazkow a iloscia ich wygenerowanych
                    
                $this->response["count_images"] = count(scandir($dir, SCANDIR_SORT_NONE));
                $this->response["count_images_all"] = count(scandir($dir, SCANDIR_SORT_NONE))*count($type);
                
                $this->response["test_Warunek_1"] = "OK";
                if (preg_match('/^[0-9]*\.jpg$/', $image)) {
                    foreach ($type as $k => $imageType) {
                       
                        // startujemy generowanie obrazkow od poczatku tj od 0
                        // Customizable writing dir
                        
                        $newDir = $dir;
                        if (!file_exists($newDir)) {
                            continue;
                        }

                        if (($dir == _PS_CAT_IMG_DIR_) && ($imageType['name'] == $formated_medium) && is_file(_PS_CAT_IMG_DIR_ . str_replace('.', '_thumb.', $image))) {
                            $image = str_replace('.', '_thumb.', $image);
                            $this->context->cookie->log .= "image".$image."<br/>";
                        }

                        if (!file_exists($newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!file_exists($dir . $image) || !filesize($dir . $image)) {
                                $this->errors[] = $this->trans('Source file does not exist or is empty (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            } elseif (!ImageManager::resize($dir . $image, $newDir . substr(str_replace('_thumb.', '.', $image), 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->errors[] = $this->trans('Failed to resize image file (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($dir . $image, $newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->errors[] = $this->trans('Failed to resize image file to high resolution (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                                }
                            }
                        }
                        // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
                        if (time() - $this->start_time > $this->max_execution_time - 4) {
                            return 'timeout';
                        }
                        
                        #echo "Generuje obrazek#1: ".substr($image, 0, -4) . '-'.$imageType['name'].".jpg \n #".$this->context->cookie->counter_others;
                        if(is_numeric(substr($image, 0, -4)) && ($k == 0)) {
                            $this->z++;
                            $this->response["zet"] = $this->z;
                            $this->response["message"][] = "Generuje obrazek(# ".$this->typ." : ".substr($image, 0, -4).")".$this->z." / ".$this->getCountImageByType($this->typ)." ".$this->typ." \n";
                            
                            // jezeli dany typ obrazka ma wiecej zdjec niz 10
                            if($this->getCountImageByType($this->typ) > 10) {
                                //to co 10 iteracji zrzucaj response
                                if($this->z % 10 == 0) { 
                                    (int)$counter=(int)$counter+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter"] = $this->i;
                                    
                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            } 
                            // jesli ilosc obrazkow jest mniejsza niz 10 zrob wszystkie w jednej iteracji i zwroc response
                            else {
                                while($this->z>=$this->getCountImageByType($this->typ)) {
                                    (int)$counter=(int)$counter+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter"] = $this->i;

                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            }
                        }
                }
                }

                    }
                }
                     else {
                         
                         if($this->i > $counter) { 
                         $this->response["test_all"] = "OK 1";
                      ////ilosc zdjec jest parzysta lub nie
                 //$this->i++;
                     // musi byc kontrola pomiedzy calkowita iloscia obrazkow a iloscia ich wygenerowanych
                    
                $this->response["count_images"] = count(scandir($dir, SCANDIR_SORT_NONE));
                $this->response["count_images_all"] = count(scandir($dir, SCANDIR_SORT_NONE))*count($type);
                
                $this->response["test_Warunek_1"] = "OK";
                if (preg_match('/^[0-9]*\.jpg$/', $image)) {
                    foreach ($type as $k => $imageType) {
                       
                        // startujemy generowanie obrazkow od poczatku tj od 0
                        // Customizable writing dir
                        
                        $newDir = $dir;
                        if (!file_exists($newDir)) {
                            continue;
                        }

                        if (($dir == _PS_CAT_IMG_DIR_) && ($imageType['name'] == $formated_medium) && is_file(_PS_CAT_IMG_DIR_ . str_replace('.', '_thumb.', $image))) {
                            $image = str_replace('.', '_thumb.', $image);
                            $this->context->cookie->log .= "image".$image."<br/>";
                        }

                        if (!file_exists($newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!file_exists($dir . $image) || !filesize($dir . $image)) {
                                $this->errors[] = $this->trans('Source file does not exist or is empty (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            } elseif (!ImageManager::resize($dir . $image, $newDir . substr(str_replace('_thumb.', '.', $image), 0, -4) . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->errors[] = $this->trans('Failed to resize image file (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($dir . $image, $newDir . substr($image, 0, -4) . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->errors[] = $this->trans('Failed to resize image file to high resolution (%filepath%)', array('%filepath%' => $dir . $image), 'Admin.Design.Notification');
                                }
                            }
                        }
                        // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
                        if (time() - $this->start_time > $this->max_execution_time - 4) {
                            return 'timeout';
                        }
                        
                        #echo "Generuje obrazek#1: ".substr($image, 0, -4) . '-'.$imageType['name'].".jpg \n #".$this->context->cookie->counter_others;
                        if(is_numeric(substr($image, 0, -4)) && ($k == 0)) {
                            $this->z++;
                            $this->response["zet"] = $this->z;
                            $this->response["message"][] = "Generuje obrazek(# ".$this->typ." : ".substr($image, 0, -4).")".$this->z." / ".$this->getCountImageByType($this->typ)." ".$this->typ." \n";
                            
                            // jezeli dany typ obrazka ma wiecej zdjec niz 10
                            if($this->getCountImageByType($this->typ) > 10) {
                                //to co 10 iteracji zrzucaj response
                                if($this->z % 10 == 0) { 
                                    (int)$counter=(int)$counter+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter"] = $this->i;
                                    
                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            } 
                            // jesli ilosc obrazkow jest mniejsza niz 10 zrob wszystkie w jednej iteracji i zwroc response
                            else {
                                while($this->z>=$this->getCountImageByType($this->typ)) {
                                    (int)$counter=(int)$counter+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter"] = $this->i;

                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            }
                        }
                }
                }
                         }
                }
                     $this->i++;
                   
            }
        } 
        // Produkty
        else {
            //  $this->response["test5"] ="OK";
            foreach (Image::getAllImages() as $key => $image) {
                $imageObj = new Image($image['id_image']);
                
                if(($this->getCountImageByType($this->typ) % 10) == 0) {

                if($this->j >= $counter_product) {
                //$this->response["test5_a"] ="OK";
                
                $existing_img = $dir . $imageObj->getExistingImgPath() . '.jpg';
               // echo "img".$imageObj;
                if (file_exists($existing_img) && filesize($existing_img)) {
                    foreach ($type as $k => $imageType) {
                        
                        if (!file_exists($dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->errors[] = $this->trans(
                                    'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                    array(
                                        '%filename%' => $existing_img,
                                        '%id%' => (int) $imageObj->id_product,
                                    ),
                                    'Admin.Design.Notification'
                                );
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->errors[] = $this->trans(
                                        'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                        array(
                                            '%filename%' => $existing_img,
                                            '%id%' => (int) $imageObj->id_product,
                                        ),
                                        'Admin.Design.Notification'
                                    );
                                }
                            }
                        }
                        
                        
                        
                        
                          if(is_numeric((int) $imageObj->id_product) && ($k == 0)) {
                            $current_typ=$this->typ; 
                            $this->y++;
                            $this->context->cookie->$current_typ += 1; 
                            $this->response["message"][] = "Generuje obrazek(# ".$this->typ." ID: ".$imageObj->id_product.")".$this->y." / ".$this->getCountImageByType($this->typ)." ".$this->typ." \n";
                            $this->response['process'][$this->typ] = (int)$this->context->cookie->$current_typ;
                            
                             $this->response["igrek"] = $this->y;
                           
                            
                            // jezeli dany typ obrazka ma wiecej zdjec niz 10
                            if($this->getCountImageByType($this->typ) > 10) {
                                //to co 10 iteracji zrzucaj response
                                if($this->y % 10 == 0) { 
                                    (int)$counter_product=(int)$counter_product+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter_product"] = $this->j;
                                    
                                    $this->response["count_type"] = count($type);
 $this->response["badam"] = "countghfghfghf";
                                    echo json_encode($this->response);
                                    exit();
                                }
                            } 
                            // jesli ilosc obrazkow jest mniejsza niz 10 zrob wszystkie w jednej iteracji i zwroc response
                            else {
                                while($this->y>=$this->getCountImageByType($this->typ)) {
                                    (int)$counter_product=(int)$counter_product+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter_product"] = $this->j;

                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            }
                            
                        }
                        
                        //    $this->response["message"][] = "Generuje obrazek#2: ".$imageObj->getExistingImgPath() . '-'.$imageType['name'].".jpg \n";
                            #echo "Generuje obrazek#2: ".$imageObj->getExistingImgPath() . '-'.$imageType['name'].".jpg \n #".$this->context->cookie->counter;
                    }
                    
                } else {
                    $this->errors[] = $this->trans(
                        'Original image is missing or empty (%filename%) for product ID %id%',
                        array(
                            '%filename%' => $existing_img,
                            '%id%' => (int) $imageObj->id_product,
                        ),
                        'Admin.Design.Notification'
                    );
                }
                if (time() - $this->start_time > $this->max_execution_time - 4) { // stop 4 seconds before the tiemout, just enough time to process the end of the page on a slow server
                    return 'timeout';
                }
  
            
                //.$imageObj->id_product.
            $this->response["message"][] = "";//Generuje obrazki dla produktu \n
            

//               if((($this->j) % count($type)) == count($type)-1) {
//                            //$this->context->cookie->counter+=10;
//                            (int)$counter_product=(int)$counter_product+(int)count($type);
//                            $this->response["counter_product"] = $counter_product;
//
//                            echo json_encode($this->response);
//                            exit();
//                        }
            }
                }
                else {
                          if($this->j > $counter_product) {
                //$this->response["test5_a"] ="OK";
                
                $existing_img = $dir . $imageObj->getExistingImgPath() . '.jpg';
               // echo "img".$imageObj;
                if (file_exists($existing_img) && filesize($existing_img)) {
                    foreach ($type as $k => $imageType) {
                        
                        if (!file_exists($dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg')) {
                            if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '.jpg', (int) $imageType['width'], (int) $imageType['height'])) {
                                $this->errors[] = $this->trans(
                                    'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                    array(
                                        '%filename%' => $existing_img,
                                        '%id%' => (int) $imageObj->id_product,
                                    ),
                                    'Admin.Design.Notification'
                                );
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($existing_img, $dir . $imageObj->getExistingImgPath() . '-' . stripslashes($imageType['name']) . '2x.jpg', (int) $imageType['width'] * 2, (int) $imageType['height'] * 2)) {
                                    $this->errors[] = $this->trans(
                                        'Original image is corrupt (%filename%) for product ID %id% or bad permission on folder.',
                                        array(
                                            '%filename%' => $existing_img,
                                            '%id%' => (int) $imageObj->id_product,
                                        ),
                                        'Admin.Design.Notification'
                                    );
                                }
                            }
                        }
                        
                        
                        
                        
                          if(is_numeric((int) $imageObj->id_product) && ($k == 0)) {
                            $current_typ=$this->typ; 
                            $this->y++;
                            $this->context->cookie->$current_typ += 1; 
                            $this->response["message"][] = "Generuje obrazek(# ".$this->typ." ID: ".$imageObj->id_product.")".$this->y." / ".$this->getCountImageByType($this->typ)." ".$this->typ." \n";
                            $this->response['process'][$this->typ] = (int)$this->context->cookie->$current_typ;
                            
                             $this->response["igrek"] = $this->y;
                           
                            
                            // jezeli dany typ obrazka ma wiecej zdjec niz 10
                            if($this->getCountImageByType($this->typ) > 10) {
                                //to co 10 iteracji zrzucaj response
                                if($this->y % 10 == 0) { 
                                    (int)$counter_product=(int)$counter_product+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter_product"] = $this->j;
                                    
                                    $this->response["count_type"] = count($type);
 $this->response["badam"] = "countghfghfghf";
                                    echo json_encode($this->response);
                                    exit();
                                }
                            } 
                            // jesli ilosc obrazkow jest mniejsza niz 10 zrob wszystkie w jednej iteracji i zwroc response
                            else {
                                while($this->y>=$this->getCountImageByType($this->typ)) {
                                    (int)$counter_product=(int)$counter_product+((int)count($type));
                                    //$this->response["counter"] = $counter;
                                    $this->response["counter_product"] = $this->j;

                                    $this->response["count_type"] = count($type);

                                    echo json_encode($this->response);
                                    exit();
                                }
                            }
                            
                        }
                        
                        //    $this->response["message"][] = "Generuje obrazek#2: ".$imageObj->getExistingImgPath() . '-'.$imageType['name'].".jpg \n";
                            #echo "Generuje obrazek#2: ".$imageObj->getExistingImgPath() . '-'.$imageType['name'].".jpg \n #".$this->context->cookie->counter;
                    }
                    
                } else {
                    $this->errors[] = $this->trans(
                        'Original image is missing or empty (%filename%) for product ID %id%',
                        array(
                            '%filename%' => $existing_img,
                            '%id%' => (int) $imageObj->id_product,
                        ),
                        'Admin.Design.Notification'
                    );
                }
                if (time() - $this->start_time > $this->max_execution_time - 4) { // stop 4 seconds before the tiemout, just enough time to process the end of the page on a slow server
                    return 'timeout';
                }
  
            
                //.$imageObj->id_product.
            $this->response["message"][] = "";//Generuje obrazki dla produktu \n
            

//               if((($this->j) % count($type)) == count($type)-1) {
//                            //$this->context->cookie->counter+=10;
//                            (int)$counter_product=(int)$counter_product+(int)count($type);
//                            $this->response["counter_product"] = $counter_product;
//
//                            echo json_encode($this->response);
//                            exit();
//                        }
            }
                }
            $this->j++;
            }
       // }
        }

        return (bool) count($this->errors);
    }
    
     /**
     * Delete resized image then regenerate new one with updated settings.
     *
     * @param string $dir
     * @param array $type
     * @param bool $product
     *
     * @return bool
     */
    protected function _deleteOldImages($dir, $type, $product = false)
    {
        if (!is_dir($dir)) {
            return false;
        }
        $toDel = scandir($dir, SCANDIR_SORT_NONE);

        foreach ($toDel as $d) {
            foreach ($type as $imageType) {
                if (preg_match('/^[0-9]+\-' . ($product ? '[0-9]+\-' : '') . $imageType['name'] . '\.jpg$/', $d)
                    || (count($type) > 1 && preg_match('/^[0-9]+\-[_a-zA-Z0-9-]*\.jpg$/', $d))
                    || preg_match('/^([[:lower:]]{2})\-default\-' . $imageType['name'] . '\.jpg$/', $d)) {
                    if (file_exists($dir . $d)) {
                        unlink($dir . $d);
                    }
                }
            }
        }

        // delete product images using new filesystem.
        if ($product) {
            $productsImages = Image::getAllImages();
            foreach ($productsImages as $image) {
                $imageObj = new Image($image['id_image']);
                $imageObj->id_product = $image['id_product'];
                if (file_exists($dir . $imageObj->getImgFolder())) {
                    $toDel = scandir($dir . $imageObj->getImgFolder(), SCANDIR_SORT_NONE);
                    foreach ($toDel as $d) {
                        foreach ($type as $imageType) {
                            if (preg_match('/^[0-9]+\-' . $imageType['name'] . '\.jpg$/', $d) || (count($type) > 1 && preg_match('/^[0-9]+\-[_a-zA-Z0-9-]*\.jpg$/', $d))) {
                                if (file_exists($dir . $imageObj->getImgFolder() . $d)) {
                                    unlink($dir . $imageObj->getImgFolder() . $d);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
        /**
     * Init display for the thumbnails regeneration block.
     */
    public function initRegenerate()
    {
        $types = array(
            'categories' => $this->trans('Categories', array(), 'Admin.Global'),
            'manufacturers' => $this->trans('Manufacturers', array(), 'Admin.Global'),
            'suppliers' => $this->trans('Suppliers', array(), 'Admin.Global'),
            'stores' => $this->trans('Stores', array(), 'Admin.Global'),
            'products' => $this->trans('Products', array(), 'Admin.Global'),
        );

        $formats = array();
        foreach ($types as $i => $type) {
            $formats[$i] = ImageType::getImagesTypes($i);
        }

        $this->context->smarty->assign(array(
            'types' => $types,
            'formats' => $formats,
        ));
    }
}
