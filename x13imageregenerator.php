<?php
/**
 * Main class of module x13imageregenerator
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

if (!defined('X13_PRODUCTDATE_ION_VERSION')) {
    if (PHP_VERSION_ID >= 70100) {
        $x13IonVer = '-71';
        $x13IonFolder = 'php71';
    } else if (PHP_VERSION_ID >= 70000) {
        $x13IonVer = '-7';
        $x13IonFolder = 'php70';
    } else {
        $x13IonVer = '';
        $x13IonFolder = 'php5';
    }

    if (file_exists(_PS_MODULE_DIR_ . 'x13imageregenerator/dev')) {
        $x13IonVer = '';
        $x13IonFolder = 'php5';
    }

    define('X13_PRODUCTDATE_ION_VERSION', $x13IonVer);
    define('X13_PRODUCTDATE_ION_FOLDER', $x13IonFolder);
}

require_once _PS_MODULE_DIR_ . 'x13imageregenerator/x13imageregenerator.core' . X13_PRODUCTDATE_ION_VERSION . '.php';

class X13imageregenerator extends X13imageregeneratorCore
{
    public function __construct()
    {
        $this->name = 'x13imageregenerator';
        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'x13.pl';
        $this->bootstrap = true;

        parent::__construct();
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('X13 Image Regenerator');
        $this->description = $this->l('Module can regenerate thumbnails in your Prestashop');
    }

    
    public function getContent()
    {
        return parent::initContent();
    }
}
