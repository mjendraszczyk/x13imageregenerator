<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_b2d37ae1cedf42ff874289b721860af2'] = 'Logi';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_d560c1e507766e4c4c287b4e95258015'] = 'Podaj starą domenę';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_3fe34462f4a0246283ecad724fc07b14'] = 'Podaj nową domenę';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_650be61892bf690026089544abbd9d26'] = 'Tryb';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_25c77df6fcbe4163f3f29f6404342a6f'] = 'W trybie Standard zmiany będą jedynie w następujcych tabelach';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_fa22eb5dc8892357062bb670c14725f2'] = 'W zaawansowanym zmiany będą dotyczyć całej bazy';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_5850263f09b8aa3bf47d108bbb1fafcc'] = 'Domena zmieniona';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_e0171416ad58e8ea5c4499ad3225c770'] = 'Ustaw domenę która ma być zmieniona';
$_MODULE['<{x13imageregenerator}prestashop>x13imageregenerator_3fe34462f4a0246283ecad724fc07b14'] = 'Ustaw nową domenę';
