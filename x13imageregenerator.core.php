<?php
/**
 * Main class of module x13imageregenerator
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 */

@ini_set('max_execution_time', 0);

class X13imageregeneratorCore extends Module
{
    protected $config_form = false;
    private $ionErrors = array();

    public function install()
    {
//        if ($this->hasIonErrors()) {
//            $this->_errors[] = $this->l('Invalid license, contact us for more details.', 'x13imageregenerator.core');
//            return false;
//        }

        Configuration::updateValue('x13imageregenerator_token', Tools::getAdminToken(time()));
        return parent::install() 
                && $this->registerHook('actionAdminControllerSetMedia')
                && $this->registerHook('DisplayHeader')
                && $this->installModuleTab('AdminImageregenerator', array(Configuration::get('PS_LANG_DEFAULT') => 'X13 Image Regenerator'), Tab::getIdFromClassName('AdminAdvancedParameters'));
    }

    public function initContent()
    {
        return Tools::redirectAdmin($this->context->link->getAdminLink('AdminImageregenerator'));
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save()) {
            return false;
        }
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
    }
  public function getcurrentajaxprocess() {
        echo "init logs";
        //return json_encode($test);
        echo $this->context->cookie->logs;
    }
    public function getcurrentajaxpercent() {
   
//         echo "init percent";
//         $this->context->cookie->test = "gfdgfdg";
//         echo "TTT".$this->context->cookie->test;
//         echo "###".$this->context->cookie->test2." ___ ";
//        echo $_SESSION['x13regeneratePercentAll']." percent";
//        echo "<br/>";
//        echo $_SESSION['x13regeneratePercentAll2'];
    }
    public function getcurrentajaximage() {
        
    }
    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallModuleTab('AdminImageregenerator'))
        {
            return false;
        }
    }
     public function hookActionAdminControllerSetMedia($params)
    {
//        if ($this->ps_version >= '1.7') {
//            $this->context->controller->registerStylesheet('modules-' . $this->name, 'modules/' . $this->name . '/views/css/' . $this->name . '.css', array('media' => 'all', 'priority' => 150));
//        } else {
            $this->context->controller->addCSS($this->_path . 'views/css/' . $this->name . '.css');
            $this->context->controller->addJS($this->_path . 'views/js/' . $this->name . '.js');
//        }
    }
    public function hookDisplayHeader($params)
    {
            $this->context->controller->addCSS($this->_path . 'views/css/' . $this->name . '.css');
            $this->context->controller->addJS($this->_path . 'views/js/' . $this->name . '.js');
    }

    public function hasIonErrors($showErrors = false)
    {
        $extensions = @get_loaded_extensions();

        if (function_exists('ioncube_file_is_encoded') === false || ($extensions && !in_array('ionCube Loader', $extensions))) {
            $this->ionErrors[] = $this->l('Ioncube is not available. See https://x13.pl/doc/postawowe-problemy.', 'x13imageregenerator.core');
            return true;
        }

        if (!ioncube_file_is_encoded()) {
            return false;
        }

        if (ioncube_license_properties() === false) {
            $this->ionErrors[] = $this->l('Invalid license.', 'x13imageregenerator.core');
        }
        if (ioncube_license_matches_server() === false) {
            $this->ionErrors[] = $this->l('Invalid license or domain.', 'x13imageregenerator.core');
        }
        if (ioncube_license_has_expired() === true) {
            $this->ionErrors[] = $this->l('The license has expired.', 'x13imageregenerator.core');
        }
        if (count($this->ionErrors)) {
            return true;
        }

        return false;
    }

    protected function renderIonErrors()
    {
        return $this->displayError($this->ionErrors);
    }
}
